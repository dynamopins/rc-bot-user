const {driver} = require('@rocket.chat/sdk');
const axios = require('axios');

var env = require('node-env-file');
env(__dirname + '/.env');

const SSL = false;
const ROOMS = ['GENERAL'];

const HOST = process.env.HOST;
const USER = process.env.BOT_USER;
const PASS = process.env.BOT_PASS;
//const SSL = process.env.SSL;  // server uses https ?
//const ROOMS = process.env.ROOM;
const RASA_URL = process.env.RASA_URI;


var myuserid;
// this simple bot does not handle errors, different message types, server resets
// and other production situations

const runbot = async () => {
    const conn = await driver.connect({host: HOST, useSsl: SSL});
    myuserid = await driver.login({username: USER, password: PASS});
    const roomsJoined = await driver.joinRooms(ROOMS);
    console.log('joined rooms');

    // set up subscriptions - rooms we are interested in listening to
    const subscribed = await driver.subscribeToMessages();
    console.log('subscribed');

    // connect the processMessages callback
    const msgloop = await driver.reactToMessages(processMessages);
    console.log('connected and waiting for messages');
}

// callback for incoming messages filter and processing
const processMessages = async (err, message, messageOptions) => {
    if (!err) {

        // filter our own message
        if (message.u._id === myuserid) return;

        // get the channel type
        const roomType = messageOptions.roomType.charAt(0);

        //Check if channel or direct message
        if (roomType == 'c') {
            const roomname = await driver.getRoomName(message.rid);

            //check if the bot is mentioned
            if (message.msg.toLowerCase().startsWith('@' + USER)) {
                respondToUserInput(message.u._id, message.rid, message.msg.substr(BOTNAME.length + 1).trim());
            }
        }
        else {
            respondToUserInput(message.u._id, message.rid, message.msg.trim());
        }
    }
}

//responds the feedback from the rasa server to the user
const respondToUserInput = (userId, roomId, text) => {
    //send a request to the rasa server
    axios.post(RASA_URL, {
        user_id: userId,
        channel_id: roomId,
        text: text
    })
        .then(response => {
            //Send the response from rasa to a specific room
            const botResponse = response.data.text;
            driver.sendToRoomId(botResponse, roomId);
        })
        .catch(error => {
            console.log(error);
        });
}

runbot()